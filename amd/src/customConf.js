define(['jquery'], function ($) {
    var customConf = {};
    var done = false;

    //var valueButtonConf = 'none';
    var chargeMarkType = 3;
    var atomSpecifiedColor = 1;
/*
    customConf.setValueButtonConf = function(value) {
        valueButtonConf = value;
    };
*/
    customConf.setChargeMarkType = function(value) {
        chargeMarkType = value;
    };

    customConf.setAtomSpecifiedColor = function(value) {
        atomSpecifiedColor = value;
    };
    customConf.init = function() {
        $('body').on('mouseover', '.K-Chem-ComposerDialog', function (event) {
             if (event && !done) { /* eslint-disable */
                Kekule.ChemWidget.Viewer._composerDialog.__$__k__p__composer
                        .renderConfigs.moleculeDisplayConfigs.defChargeMarkType=chargeMarkType;
                
                Kekule.ChemWidget.Viewer._composerDialog.__$__k__p__composer
                        .renderConfigs.colorConfigs.useAtomSpecifiedColor=atomSpecifiedColor;
                /* eslint-disable */
                done = true;
             }
        });
    };
    window.customConf = customConf;
    return customConf;
 });