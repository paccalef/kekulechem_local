<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

/**
 * Config class kekule.js plugins
 */
class kekulejs_configs
{
    //const DEF_MOL_COMPARER_URL = 'http://127.0.0.1:3000/mols/compare';

    const DEF_KEKULE_DIR = '/local/kekulejs/';
    //const DEF_KEKULE_JS_DIR = '/local/kekule.js/scripts/';

    /**
     * Returns root dir of Kekukejs plugins.
     * @return string
     */
    static public function getKekuleDir()
    {

        $result = get_config('mod_kekule', 'kekule_dir');
        if (empty($result))
            $result = self::DEF_KEKULE_DIR;
        return $result;
    }
    /**
     * Returns dir of JavaScript files (including Kekule.js and its dependencies).
     * @return string
     */
    static public function getScriptDir()
    {
        return self::getKekuleDir() . 'jslib/';
    }
    /**
     * Returns dir of Kekule.js JavaScript files.
     * @return string
     */
    static public function getKekuleScriptDir()
    {
        return self::getScriptDir() . 'kekule.js.0.8.0.18090700/';
    }
    static public function getAdapterDir()
    {
        return self::getKekuleDir() . 'adapter/';
    }
}

class kekulejs_utils
{
    /**
     * Add essential Kekule.js Javascript files to $PAGE.
     * @param $page
     * @param null $options Array that stores options to load Kekule.js
     */
    static public function includeKekuleScriptFiles($options = null, $page = null)
    {
        global $PAGE, $CFG, $COURSE, $USER;

      /*  $p = $page;
        if (!isset($p)) {*/
            $p = $PAGE;
           // $scriptDir = kekulejs_configs::getScriptDir();
          //  $kekuleScriptDir = kekulejs_configs::getKekuleScriptDir();
          //  $adapterDir = kekulejs_configs::getAdapterDir();
//        }
        
        // params
        $params = '';
        if (isset($options)) {
            foreach ($options as $key => $value) {
                $params .= $key . '=' . $value;
            }
        } else  // use default
            $params = '?modules=chemWidget,algorithm&locals=en,zh';
        
        /*looking for get the lib by the remote git repo directly*/
        $kekulejs_configs = new kekulejs_configs();
        
        //Link to the git repo master branch
        $p->requires->js($kekulejs_configs::getScriptDir() . 'Three.js');
        $p->requires->js($kekulejs_configs::getScriptDir() . 'raphael-min.js');
        $p->requires->js($kekulejs_configs::getScriptDir() . 'Kekule.js/dist/kekule.js?' . $params);
        
        //Link to the default moodle library
        //$p->requires->js($kekulejs_configs::getScriptDir() . 'Kekule.js/src/_extras/MoodleExtensions/moodle/local/kekulejs/jslib/Three.js');
        //$p->requires->js($kekulejs_configs::getScriptDir() . 'Kekule.js/src/_extras/MoodleExtensions/moodle/local/kekulejs/jslib/kekule.js.0.8.0.18090700/kekule.js?' . $params);
        //$p->requires->js($kekulejs_configs::getScriptDir() . 'Kekule.js/src/_extras/MoodleExtensions/moodle/local/kekulejs/adapter/kekuleInitials.js');
        
        /*-------------------------------------------*/
        /*
        $p->requires->js($scriptDir . 'raphael-min.js');
        $p->requires->js($scriptDir . 'Three.js');
        $p->requires->js($kekuleScriptDir . 'kekule.js?' . $params);
        $p->requires->js($adapterDir . 'kekuleInitials.js');*/
        $context = context_course::instance($COURSE->id);
        $access_admin = user_has_role_assignment($USER->id, 1, $context->id);
        $access_student = user_has_role_assignment($USER->id, 5, $context->id);
        $access_teacher = user_has_role_assignment($USER->id, 3, $context->id);
        $access_creator = user_has_role_assignment($USER->id, 2, $context->id);
        $access_reading_teacher = user_has_role_assignment($USER->id, 4, $context->id);
        
        $PAGE->requires->js_call_amd('local_kekulejs/customConf', 'init');
        
        if ( (get_config('mod_kekule', 'circleArroundAdmin') == 1 
                &&
             ($access_admin || $access_teacher || $access_reading_teacher || $access_creator)
           )
           ||
           (get_config('mod_kekule', 'confButtonHiddenStudent') == 1
                &&
           $access_student)){
            $PAGE->requires->js_call_amd('local_kekulejs/customConf',"setChargeMarkType",array(3));
        } else {
            $PAGE->requires->js_call_amd('local_kekulejs/customConf',"setChargeMarkType",array(1));
        }
        
        if ((get_config('mod_kekule', 'specifiedColorAdmin') == 1
                &&
             ($access_admin || $access_teacher || $access_reading_teacher || $access_creator))
            ||
            (get_config('mod_kekule', 'specifiedColorStudent') == 1
                &&
            $access_student)    
           ){
            $PAGE->requires->js_call_amd('local_kekulejs/customConf',"setAtomSpecifiedColor", array(1));
        } else {
            $PAGE->requires->js_call_amd('local_kekulejs/customConf',"setAtomSpecifiedColor", array(0));
        }
        
        
        if (( get_config('mod_kekule', 'confButtonHiddenAdmin') == 1
                &&
             ($access_admin || $access_teacher || $access_reading_teacher || $access_creator)
            )
            ||
            (get_config('mod_kekule', 'confButtonHiddenStudent') == 1
                &&
            $access_student)    
           ){
            echo "<style>.K-Action-Open-Configurator {display: none !important;}</style>";
        }
        
    }
    static public function includeKekuleJsFiles($options = null, $page = null)
    {
        return kekulejs_utils::includeKekuleScriptFiles($options, $page);
    }

    /**
     * Add essential Kekule.js CSS files to $PAGE.
     * @param $page
     */
    static public function includeKekuleCssFiles($page = null)
    {
        global $PAGE;
        $p = $page;
        if (!isset($p))
            $p = $PAGE;

        $scriptDir = kekulejs_configs::getScriptDir();
		$kekuleScriptDir = kekulejs_configs::getKekuleScriptDir();
        try {
            //Link to the git repo master branch
            $p->requires->css($scriptDir . '/Kekule.js/dist/themes/default/kekule.css');
            //Link to the default moodle library
         /* $p->requires->css($kekuleScriptDir . 'themes/default/kekule.css');
            
            $kekulejs_configs = new kekulejs_configs();
            $p->requires->css($kekulejs_configs::getScriptDir() . 'Kekule.js/src/_extras/MoodleExtensions/moodle/local/kekulejs/jslib/kekule.js.0.8.0.18090700/themes/default/kekule.css');
      */  }
        catch(Exception $e)
        {
            // do nothing, just avoid exception
        }
    }

    static public function includeAdapterJsFiles($page = null)
    {
        global $PAGE;

        $p = $page;
        if (!isset($p))
            $p = $PAGE;
        $dir = kekulejs_configs::getAdapterDir();
        $p->requires->js($dir . 'kekuleMoodle.js');
    }

    /**
     * Add essential Kekule.js CSS files to $PAGE.
     * @param $page
     */
    static public function includeAdapterCssFiles($page = null)
    {
        global $PAGE;

        $p = $page;
        if (!isset($p))
            $p = $PAGE;
        $dir = kekulejs_configs::getAdapterDir();
        try {
            $p->requires->css($dir . 'kekuleMoodle.css');
        }
        catch(Exception $e)
        {
            // do nothing, just avoid exception
        }
    }
}